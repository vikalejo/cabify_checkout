require 'cabify_checkout/checkout'
require 'byebug'

describe Checkout do
  let(:service) { described_class.new(pricing_rules) }
  let(:default_prices) { { tshirt: 20, voucher: 5, mug: 7.5 } }
  let(:pricing_rules) { 'A' }

  describe '.scan' do
    subject { service.scan(item) }

    before do
      allow(service).to receive(:show_error).and_return true
      subject
    end

    context 'with a valid item' do
      let(:item) { 'TSHIRT' }
      let(:number_items) { { tshirt: 1 } }
      let(:items_cost) { { tshirt: 20 } }

      it 'scans the item successfully' do
        expect(service.items_collector).to match ["TSHIRT"]
      end

      it 'calculates the number of items' do
        expect(service.number_items).to match number_items
      end

      it 'calculates the cost of items' do
        expect(service.items_cost).to match items_cost
      end
    end

    context 'with an invalid item' do
      let(:item) { 'SHIT' }

      it 'does not scans the item' do
        expect(service.items_collector).to be_empty
      end

      it 'returns an error' do
        expect(service).to have_received(:show_error)
      end
    end

    context 'with several items' do
      let(:items) { ['mug', 'voucher', 'tshirt', 'tshirt'] }
      before do
        items.each { |i| service.scan(i) }
      end
      subject { service }

      let(:items_cost) { { mug: 7.5, tshirt: 40, voucher: 5 } }

      it 'shows partial cost for item' do
        expect(service.items_cost).to match items_cost
      end

    end
  end

  describe '.total' do
    subject { service.total }

    before do
      service.items_collector = items_collector
      service.number_items = number_items
      service.items_cost = items_cost
      allow(service).to receive(:show_total_output).and_return true
      subject
    end

    context 'with promotions not enabled' do
      let(:pricing_rules) { 'D' }
      let(:items_collector) do
        ['TSHIRT', 'TSHIRT', 'TSHIRT', 'MUG', 'MUG', 'VOUCHER']
      end
      let(:number_items) do
        {:tshirt=>3, :mug=>2, :voucher=>1}
      end
      let(:items_cost) do
        {:tshirt=>60, :mug=>15.0, :voucher=>5}
      end

      it 'set pricing rules' do
        expect(service.pricing_rules).to be false
      end

      it 'calculates the total' do
        expect(service.total_amount).to eq 80.0
        expect(service.items_cost).to match items_cost
      end
    end

    context 'with promotions enabled' do
      let(:pricing_rules) { 'A' }

      context 'with get_one_free_promotion' do
        let(:items_collector) do
          ['TSHIRT', 'MUG', 'MUG', 'VOUCHER', 'VOUCHER']
        end
        let(:items_cost) do
          {:tshirt=>20, :mug=>15.0, :voucher=>10}
        end
        let(:number_items) do
          {:tshirt=>1, :mug=>2, :voucher=>3}
        end

        it 'calculates the total' do
          expect(service.total_amount).to eq 45.0
          expect(service.items_cost).to match items_cost
        end

        it 'adds a new item' do
          expect(service.number_items).to match number_items
        end
      end

      context 'with promotion_price_reduced' do
        let(:items_collector) do
          ['TSHIRT', 'TSHIRT', 'TSHIRT', 'MUG', 'MUG', 'VOUCHER', 'VOUCHER']
        end
        let(:items_cost) do
          {:tshirt=>57, :mug=>15.0, :voucher=>10}
        end
        let(:number_items) do
          {:tshirt=>3, :mug=>2, :voucher=>3}
        end

        it 'calculates the total' do
          expect(service.total_amount).to eq 82.0
          expect(service.number_items).to match number_items
        end

        it 'reduces the price of item' do
          expect(service.items_cost).to match items_cost
        end
      end
    end
  end
end
