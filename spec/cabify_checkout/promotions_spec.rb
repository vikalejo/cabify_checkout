require 'byebug'
require 'cabify_checkout/promotions'

describe Promotions do
  let(:service) { described_class.new(checkout) }
  let(:checkout) { Checkout.new('A') }
  let(:items_cost) do
    {:tshirt=>60, :mug=>22.50, :voucher=>5}
  end
  let(:number_items) do
    {:tshirt=>3, :mug=>3, :voucher=>2}
  end

  subject { service.execute }

  before do
    checkout.number_items = number_items
    checkout.items_cost = items_cost
  end

  describe '.execute' do
    before { subject }

    describe 'get one free promotion' do
      context 'with items with one free promotion' do
        it 'add a new item in checkout' do
          expect(checkout.items_collector).to match [:voucher]
          expect(checkout.number_items[:voucher]).to eq 3
        end
      end

      context 'with items no promotion' do
        it 'does not add a new item in checkout' do
          expect(checkout.items_collector).to match [:voucher]
          expect(checkout.number_items[:mug]).to eq 3
          expect(checkout.number_items[:tshirt]).to eq 3
        end
      end

      context 'with no promotion conditions' do
        let(:number_items) do
          {:tshirt=>3, :mug=>2, :voucher=>1}
        end
        it 'does not add a new item in checkout' do
          expect(checkout.items_collector).to match []
          expect(checkout.number_items[:voucher]).to eq 1
          expect(checkout.number_items[:mug]).to eq 2
          expect(checkout.number_items[:tshirt]).to eq 3
        end
      end
    end

    context 'with an item with reduced promotion' do
      context 'with items with reduce price promotion' do
        it 'reduces price checkout' do
          expect(checkout.items_cost[:tshirt]).to eq 57
        end
      end

      context 'with items no promotion' do
        it 'does not reduce the price' do
          expect(checkout.items_cost[:mug]).to eq 22.50
        end
      end

      context 'with no promotion conditions' do
        let(:number_items) do
          {:tshirt=>2, :mug=>2, :voucher=>2}
        end
        let(:items_cost) do
          {:tshirt=>40, :mug=>22.50, :voucher=>5}
        end

        it 'does not reduce price' do
          expect(checkout.items_cost[:tshirt]).to eq 40
        end
      end
    end
  end
end

