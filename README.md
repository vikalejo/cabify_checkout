# CabifyCheckout

This is a checkout app for the Cabify store.
The app allows to calculate the total amount after scanning items.

The idea about how is this app developed:
 - Allow to scan items in different order and validate them.
 It will calculate the amount for each group of items previously to apply any promotions.

 - When call `total` it will apply the promotions and the final amount to pay will be shown.

`Promotions` management is in a different service class in order to make easier its configuration and expansion.

It allows to receive a `checkout` and calculate the promotions if any is enabled.

## Configuration

**Add a new item**:

1.  Add the new item in the `VALID_ITEMS` array.
2.  Add the item prices in the `default_prices` hash.
`{ tshirt: 20, voucher: 5, mug: 7.5 }`

**Add a new promotion for an item**:

There is a service to manage Promotions. `lib/cabify_checkout/promotions`
We can just add the item we want to the type of Promotions array.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cabify_checkout'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cabify_checkout

## Usage

Go to the console: `/bin/console`

Invoke the class and start scanning.

Promotions are enabled by default with any character in initialization.

To Disable promotions, initialize with the letter **'d'**.

```ruby
co = Checkout.new 'e'
co.scan 'MUG'
co.scan 'TSHIRT'
co.scan 'VOUCHER'
....
```

Partial results will be calculated as a normal Cart process.

To get the total and apply promotions just ask for total: `co.total`

If you want to reset the values and keep scanning: `co.reset_values`

## Expansion

There are some improvements that could be done. It could become more interactive with the user and allow to:
 - to set up the prices per item
 - to set up the promotions per item
 - to manage the items

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/cabify_checkout.
