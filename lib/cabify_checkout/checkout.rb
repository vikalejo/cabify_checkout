require 'cabify_checkout/promotions'
require 'byebug'

class Checkout
  VALID_ITEMS = ['TSHIRT', 'VOUCHER', 'MUG']

  attr_accessor :items_collector, :number_items, :items_cost, :total_amount
  attr_reader :pricing_rules, :default_prices

  def initialize(rules)
    @pricing_rules = enable_pricing_rules?(rules)
    @default_prices = { tshirt: 20, voucher: 5, mug: 7.5 }

    reset_values
  end

  def scan(item)
    if valid_item?(item)
      @items_collector << item
      get_number_items_scanned
      calculate_item_cost
    else
      show_error(item)
    end
  end

  def total
    apply_promotions
    calculate_total_cost
    show_total_output
  end

  def reset_values
    @items_collector = []
    @number_items = {}
    @items_cost = {}
    @total_amount = 0.0
  end

  private

  def valid_item?(item)
    VALID_ITEMS.include?(item.upcase.to_s)
  end

  def show_error(item)
    puts <<-invalid_item
    Item #{item} is not valid
    These are the valid Items: #{VALID_ITEMS.join(',')}"
    invalid_item
  end

  def get_number_items_scanned
    uniq_items = @items_collector.compact.uniq
    uniq_items.each do |item|
      @number_items["#{item}".downcase.to_sym] = @items_collector.count(item)
    end
  end

  def calculate_item_cost
    @number_items.each do |item, number|
      @items_cost[item] = @default_prices[item] * number
    end
    @items_cost
  end

  def calculate_total_cost
    @total_amount = 0.0
    @items_cost.each do |item, cost|
      @total_amount += cost
    end
  end

  # Initializate with 'D' will disable promotions
  def enable_pricing_rules?(rules)
    rules.to_s.upcase != 'D'
  end

  # Send the checkout to the promotions service
  def apply_promotions
    Promotions.new(self).execute if @pricing_rules
  end

  def show_total_output
    puts <<-total_output
    Items: #{@items_collector.join("," + " ").upcase}
    Total: #{'%.02f' % @total_amount} €
    total_output
  end
end
