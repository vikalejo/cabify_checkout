class Promotions
  PROMOTION_GET_ONE_FREE = ['VOUCHER']
  PROMOTION_PRICE_REDUCED = ['TSHIRT']
  AMOUNT_REDUCED = 1
  MIN_ITEMS_FREE_PROMO = 2
  MIN_ITEMS_REDUCED_PROMO = 3

  def initialize(checkout)
    @checkout = checkout
  end

  def execute
    @checkout.items_cost.each do |item, cost|
      apply_get_one_free(item) if get_one_free_promotion?(item)
      apply_price_reduced(item) if price_reduced_promotion?(item)
    end
  end

  def get_one_free_promotion?(item)
    PROMOTION_GET_ONE_FREE.include?(item.to_s.upcase) &&
      @checkout.number_items[item] >= MIN_ITEMS_FREE_PROMO
  end

  def price_reduced_promotion?(item)
    PROMOTION_PRICE_REDUCED.include?(item.to_s.upcase) &&
      @checkout.number_items[item] >= MIN_ITEMS_REDUCED_PROMO
  end

  def apply_get_one_free(item)
    @checkout.items_collector.push(item)
    @checkout.number_items[item] = @checkout.number_items[item] += 1
  end

  # Reduce the base price of the item
  def apply_price_reduced(item)
    price_reduced = @checkout.default_prices[item] - AMOUNT_REDUCED
    @checkout.items_cost[item] = @checkout.number_items[item] * price_reduced
  end
end
